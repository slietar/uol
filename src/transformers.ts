import { EOL } from 'os';
import { Duplex, Transform } from 'stream';

import { getColor, LevelLike } from './levels';
import { Logger } from './logger';
import { filterNamespace, NamespacesLike, parseNamespaces } from './namespace';


export class ConcatTransformer extends Transform {
  constructor() {
    super({
      writableObjectMode: true,

      transform: (chunk: Buffer, _encoding, callback) => {
        this.push(chunk.toString() + EOL);
        callback();
      }
    });
  }
}


export type TransformerHandler = (event: any, logger: Logger) => Promise<any> | any;

export class TransformerStream extends Duplex {
  #logger: Logger;

  constructor(handler: TransformerHandler, logger: Logger) {
    super({
      objectMode: true,
      read: () => {},
      write: (chunk, _encoding, callback) => {
        Promise.resolve(handler(chunk, logger)).then((output) => {
          if (Array.isArray(output)) {
            for (let message of output) {
              this.push(message);
            }
          } else if (output) {
            this.push(output);
          }

          callback();
        }, (err) => {
          callback(err);
        });
      }
    });

    this.#logger = logger;

    this.on('finish', () => {
      this.push(null);
    });
  }

  use(handler: TransformerHandler) {
    return this.pipe(new TransformerStream(handler, this.#logger));
  }
}


export function filter(options?: {
  level?: LevelLike;
  namespace?: NamespacesLike;
}) {
  let namespaces = options?.namespace !== undefined
    ? parseNamespaces(options.namespace)
    : null;

  return ((event: any, logger: Logger) => {
    let level = options?.level !== undefined
      ? logger.getLevel(options.level)
      : null;

    if ((level !== null) && (event.level > level)) {
      return null;
    }

    if (namespaces && !namespaces.some((namespace) => filterNamespace(event.namespace, namespace))) {
      return null;
    }

    return event;
  }) satisfies TransformerHandler;
}


export interface FormatVariable {
  name: string;
  pad: 'left' | 'right' | null;
}

export function format(format: string = '%(levelColorOpen)%(LevelName:r)%(levelColorClose) %(namespace:r) %(message)') {
  let regexp = /%\(([a-zA-Z]+)(?::([lr])?)?\)/g;
  let match;

  let fragments: (FormatVariable | string)[] = [];
  let pads = new Map<FormatVariable, number>();
  let lastIndex = 0;

  while ((match = regexp.exec(format)) !== null) {
    let pad = ({ 'l': 'left', 'r': 'right' } as const)[match[2]] ?? null;

    let variable = {
      name: match[1],
      pad
    };

    fragments.push(format.slice(lastIndex, match.index), variable);

    if (pad) {
      pads.set(variable, 0);
    }

    lastIndex = regexp.lastIndex;
  }

  fragments.push(format.slice(lastIndex));


  return ((event: any, logger: Logger) => {
    let levelName = logger.levels[event.level];
    let levelNameStyle = getColor(levelName);

    let variableValues: Record<string, string> = {
      get date() { return (event.date as Date).toISOString(); },
      get level() { return (event.level as number).toString(); },
      get levelColorClose() { return (levelNameStyle?.close ?? ''); },
      get levelColorOpen() { return (levelNameStyle?.open ?? ''); },
      get levelName() { return levelName; },
      get LevelName() { return levelName.toUpperCase(); },
      get message() { return event.message; },
      get namespace() { return event.namespace.join('.') || '<root>'; },
    };

    return fragments
      .map((fragment, index) => {
        if ((index % 2) === 0) {
          return fragment;
        }

        let variable = fragment as FormatVariable;
        let value = variableValues[variable.name as keyof typeof variableValues];

        if (!variable.pad) {
          return value;
        }

        let padValue = pads.get(variable)!;

        if (padValue < value.length) {
          padValue = Math.floor(value.length * 1.2);
          pads.set(variable, padValue);
        }

        switch (variable.pad) {
          case 'left': return value.padStart(padValue, ' ');
          case 'right': return value.padEnd(padValue, ' ');
        }
      })
      .join('');
  }) satisfies TransformerHandler;
}
