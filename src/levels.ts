import ansiStyles, { CSPair } from 'ansi-styles';


export type Levels = readonly string[];
export type LevelLike = number | string;

export type CallableLevels<T extends Levels> = {
  [Prop in keyof T as (Prop extends number ? T[Prop] : never)]: (message: string) => void;
}

export namespace StdLevels {
  export const Npm = ['error', 'warn', 'http', 'verbose', 'debug', 'silly'] as const satisfies Levels;
  export const Syslog = ['emerg', 'alert', 'crit', 'error', 'warning', 'notice', 'info', 'debug'] as const satisfies Levels;
  export const Python = ['critical', 'error', 'warning', 'info', 'debug'] as const satisfies Levels;
}


const colorMap = new Map<string[], string>([
  [['alert', 'crit', 'emerg', 'err'], 'red'],
  [['warn'], 'yellow'],
  [['info'], 'blue'],
  [['notice'], 'white']
])

export function getColor(levelName: string) {
  for (let [keys, value] of colorMap) {
    if (keys.some((key) => levelName.startsWith(key))) {
      return (ansiStyles as unknown as Record<string, CSPair>)[value];
    }
  }

  return null;
}
