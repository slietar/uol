import { PassThrough } from 'stream';

import { CallableLevels, LevelLike, Levels } from './levels';
import { Namespace, NamespaceLike, parseNamespace } from './namespace';
import { TransformerHandler, TransformerStream } from './transformers';


export type LoggerWithLevels<T extends Levels> = Logger<T> & CallableLevels<T>;

export class Logger<T extends Levels = Levels> extends PassThrough {
  readonly levels: T;
  readonly namespace: Namespace;

  private children: Record<string, LoggerWithLevels<T>> = {};

  constructor(options: {
    levels: T;
    namespace?: NamespaceLike;
  }) {
    super({
      objectMode: true,
      read: () => {}
    });

    this.levels = options.levels;
    this.namespace = parseNamespace(options.namespace ?? []);

    return new Proxy<this>(this, {
      get: (_target, prop) => {
        if (typeof prop === 'string') {
          let level = this.levels.indexOf(prop);

          if (level >= 0) {
            return this.log.bind(this, level);
          }
        }

        return Reflect.get(this, prop);
      },
      ownKeys: (_target) => {
        return [...Object.keys(this.levels), ...Object.keys(this)];
      }
    });
  }

  getChild(childNamespace: NamespaceLike): LoggerWithLevels<T> {
    let relativeNamespace = parseNamespace(childNamespace);

    if (relativeNamespace.length < 1) {
      return this.init();
    }

    let [firstSegment, ...restNamespace] = relativeNamespace;

    if (this.children[firstSegment]) {
      return this.children[firstSegment].getChild(restNamespace);
    }

    let logger = new Logger({
      levels: this.levels,
      namespace: [...this.namespace, firstSegment]
    });

    this.children[firstSegment] = logger.init();
    logger.pipe(this);

    return logger.getChild(restNamespace);
  }

  getLevel(level: LevelLike) {
    return typeof level === 'string'
      ? this.levels.indexOf(level)
      : level;
  }

  init() {
    return this as LoggerWithLevels<T>;
  }

  log(level: number | T[number], message: string) {
    this.push({
      date: new Date(),
      level: typeof level === 'string'
        ? this.levels.indexOf(level)
        : level,
      message,
      namespace: this.namespace
    });
  }

  // end() {
  //   this.push(null);
  // }

  use(handler: TransformerHandler): TransformerStream {
    return this.pipe(new TransformerStream(handler, this));
  }
}
