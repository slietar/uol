export type Namespace = string[];
export type NamespaceLike = Namespace | string;
export type NamespacesLike = NamespaceLike | NamespaceLike[];

export function filterNamespace(target: Namespace, filter: Namespace): boolean {
  return filter.slice(0, target.length).every((segment, index) => target[index] === segment);
}

export function parseNamespace(input: NamespaceLike): Namespace {
  input ||= [];

  return typeof input === 'string'
    ? input.split('.')
    : input;
}

export function parseNamespaces(input: NamespacesLike): Namespace[] {
  return typeof input === 'string'
    ? [parseNamespace(input)]
    : input.map((ns) => parseNamespace(ns));
}
