# Uol

Uol is a lightweight logging utility taking advantage on Node.js streams.


## Installation

```sh
$ npm install uol
```


## Usage

### Example

```js
import * as uol from 'uol';

// Creating a logger
let logger = new uol.Logger({
  // Predefined levels
  levels: uol.StdLevels.Npm,

  // Custom levels
  levels: ['emergency', 'error', 'warning', 'info', 'debug']
}).init(); // <- init() returns an object with better typing, but is otherwise a no-op

// Declaring messages (with the custom levels defined above)
logger.emergency('Emergency message');
logger.error('Error message');
logger.warning('Warning message');
logger.info('Info message');
logger.debug('Debug message');

// Creating child loggers
let child = logger.getChild('foo');
let subchild = child.getChild('bar');

subchild.debug('...');

// Displaying and saving logs
let transport = logger
  .use(uol.format('%(date) [%(LevelName)] %(message)'))
  .use(uol.filter({ level: 'warning', namespace: 'foo.bar' }))
  .pipe(new uol.ConcatTransformer())
  .pipe(fs.createWriteStream('out.log'));
  // or
  .pipe(process.stderr);
```

### Waiting for logs to be written

```js
transport.on('finish', () => {
  // Done writing
});

logger.end();
```


## Reference

### Levels

- `StdLevels.Npm` – npm logging levels (error, warn, info, http, verbose, debug, silly)
- `StdLevels.Python` – Python [logging module](https://docs.python.org/3/library/logging.html) levels (critical, error, warning, info, debug)
- `StdLevels.Syslog` – RFC 5424 levels (emerg, alert, crit, error, warning, notice, info, debug)

### Transformers

- `filter({ level, namespace })` – Filters events depending on their level and namespace.
- `format(format)` – Formats events using a defined format.
- `ConcatTransformer()` – Joins all events (assuming they are strings) with an EOL character.
